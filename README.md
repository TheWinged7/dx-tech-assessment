# DX Tech Assessment - Nicholas Valiente

## Run All
### Requirements
    - Docker
    - Docker-compose
### Running
`docker-compose run tests`

## Palindrome checker
### Requirements
 - Python 3.6+
### Running
`python check_palindromes.py [-i|-s] <items to check>`
### Arguments
```
-i --int: check a comma separated list of integers for palindrome status
-s --string: check a comma separated list of strings for palindrome status
```
### Example Usage
#### Command
`python check_palindromes.py -i 1,123,1234,1221,121,111111`
#### Expected output
```
Number '1' is a palindrome!
Number '123' is not a palindrome!
Number '1234' is not a palindrome!
Number '1221' is a palindrome!
Number '121' is a palindrome!
Number '111111' is a palindrome!
```


### Running Unit Tests
From the `palindromes` folder run:

 `make test`

or from the root directory run:

`make -f ./palindromes/Makefile test`

## Herokuapp Automation Tests
### Requirements
 - Python 3.6+
 - Python Selenium package
 - Python pytest package
 - Chromedriver 78.0+

### Quick setup
Assuming that you have Python 3.6 or higher installed you can use the included Makefile to quickly setup your environment by running from the `web_automation` folder:

`make setup`

or from the project root:

`make -f web_automation/Makefile setup`

### Running
From the `web_automation` folder run:

 `make test`

or from the project root:

`make -f web_automation/Makefile test`

