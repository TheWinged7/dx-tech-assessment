from palindromes.palindrome import Palindromes


class String_palindrome(Palindromes):

    def check_palindrome(self, word):
        if word:
            return word == word[::-1] 
        return False
