import argparse
import sys
from palindromes.integer_palindrome import Integer_palindrome as ip
from palindromes.string_palindrome import String_palindrome as sp


if __name__ == "__main__":
    string_checker = sp()
    int_checker = ip()
    integers = []
    strings = []

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-s', '--string', help="Comma separated list of words to be checked")
    parser.add_argument(
        '-i', '--int', help="Comma separated list of integers to be checked")
    args = parser.parse_args()

    if args.string:
        strings = [word for word in args.string.split(",")]
        for word in strings:
            message = "String '{word}' {status} a palindrome!"
            status = "is" if string_checker.check_palindrome(
                word) else "is not"
            print(message.format(word=word, status=status))

    elif args.int:
        try:
            integers = [int(word) for word in args.int.split(",")]
        except ValueError as err:
            print(err)
            parser.print_help()
            exit(2)

        for num in integers:
            message = "Number '{num}' {status} a palindrome!"
            status = "is" if int_checker.check_palindrome(num) else "is not"
            print(message.format(num=num, status=status))

    else:
        parser.print_help()
        exit(2)
