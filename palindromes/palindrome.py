from abc import ABC, abstractmethod

class Palindromes(ABC):

    def __init__(self):
        super().__init__()

    @abstractmethod
    def check_palindrome(self, word):
        pass