import unittest
import json
from palindromes.integer_palindrome import Integer_palindrome as ip
from palindromes.string_palindrome import String_palindrome as sp

class TestPalindromes(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        with open("palindromes/testdata/string_tests.json", "r") as f:
            cls.string_cases = json.load(f)
        with open("palindromes/testdata/integer_tests.json", "r") as f:
            cls.int_cases = json.load(f)
        
        cls.string_checker = sp()
        cls.int_checker = ip()

    def test_integer_checker(self):
        for case in self.int_cases:
            test_data = self.int_cases.get(case)

            results = [self.int_checker.check_palindrome(word) for word in test_data.get("input")]
            try:
                self.assertListEqual(results ,test_data.get("expected"))
            except AssertionError as err:
                message = "Failure in int_checker tests for test case: '{test_case}'..."
                print(message.format(test_case=case))
                raise err
    
    def test_string_checker(self):
        for case in self.string_cases:
            test_data = self.string_cases.get(case)

            results = [self.string_checker.check_palindrome(word) for word in test_data.get("input")]
            try:
                self.assertListEqual(results ,test_data.get("expected"))
            except AssertionError as err:
                message = "Failure in string_checker tests for test case: '{test_case}'..."
                print(message.format(test_case=case))
                raise err
        

if __name__ == "__main__":
    unittest.main()
    