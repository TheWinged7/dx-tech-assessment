from palindromes.palindrome import Palindromes


class Integer_palindrome(Palindromes):

    def check_palindrome(self, word):
        if word and type(word) is int:
            return str(word) == str(word)[::-1]
        return False
