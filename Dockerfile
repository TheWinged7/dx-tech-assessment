FROM selenium/standalone-chrome

RUN sudo apt-get update  
RUN sudo apt-get install python3.7 -y
RUN sudo apt-get install python3.7-venv -y 
RUN sudo apt-get install python3-pip -y 
RUN sudo apt-get install make -y

ENV VIRTUAL_ENV=/opt/venv
RUN sudo python3.7 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
COPY ./web_automation/requirements.txt requirements.txt
RUN sudo /opt/venv/bin/pip install -r requirements.txt

