from contextlib import contextmanager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import staleness_of

class PageWaiter():
    @contextmanager
    def wait_for_page_load(self, driver, timeout=30):
        old_page = driver.find_element_by_tag_name('html')
        yield
        WebDriverWait(driver, timeout).until(staleness_of(old_page))