import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from web_automation.page_waiter import PageWaiter
from selenium.webdriver.common.alert import Alert

def test_herokuapp():
    # helper class that waits until new page has started loading
    page_waiter = PageWaiter()
    
    options = Options()
    options.headless = True

    driver = webdriver.Chrome(options=options)
    driver.get("https://the-internet.herokuapp.com/")

    try:
        content_segment = driver.find_element_by_id('content')
        
        # find JavaScript Alerts link by text and click
        with page_waiter.wait_for_page_load(driver):
            content_segment.find_element_by_link_text('JavaScript Alerts').click()

        content_segment = driver.find_element_by_id('content')
        prompt_button = content_segment.find_element_by_xpath(
            "//button[contains(text(), 'Click for JS Confirm')]")

        prompt_button.click()

        alert = driver.switch_to.alert
        alert.accept()
        
        result = content_segment.find_element_by_id('result')

        assert result.text == "You clicked: Ok"

    except Exception as err:
        driver.close()
        raise err


    driver.close()


if __name__ == "__main__":
    test_herokuapp()
    pass