PIP3=$(shell which pip3)
ifeq (, $(PIP3))
$(error "pip not found. Please install pip first")
endif

REQS=$(shell pip list | grep selenium)
ifeq (, $(REQS))
$(error "requirements not installed...")
endif


web_test:
	pytest web_automation/test_herokuapp.py; 

palindrome_test:
	python -m unittest palindromes/test_*.py

run:  palindrome_test web_test